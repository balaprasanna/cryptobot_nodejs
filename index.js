'use strict';
 
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');

var express = require("express");
var port = process.env.PORT || 3000;
var app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());


app.post("/", function (request, response) {
   // res.send(JSON.stringify({ Hello: ‘World’}));

  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
 
  function welcome(agent) {
    agent.add(`Welcome to my agent!`);
  }

  function getPrice(agent) {
    let coinname = request.body["queryResult"]["parameters"]["any"]
    agent.add(`The price of ${coinname} is $USD 6666.70, This is hardcodes in nodejs backend`);
  }
 
  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
}

  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('AskPriceIntent', getPrice);
  // intentMap.set('your intent name here', googleAssistantHandler);
  agent.handleRequest(intentMap);


});

app.listen(port, function () {
 console.log(`Example app listening on port !`, port);
});